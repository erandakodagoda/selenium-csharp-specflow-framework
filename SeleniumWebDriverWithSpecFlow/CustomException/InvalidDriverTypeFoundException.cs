﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumWebDriverWithSpecFlow.CustomException
{
    /**
        *  @author : Eranda Kodagoda
        *  @date : September 01, 2020
        *  @version : 1.0
        *  @copyright : © 2020 Eranda Kodagoda
    *   */
    public class InvalidDriverTypeFoundException : Exception
    {
        public InvalidDriverTypeFoundException(string message) : base(message)
        {

        }
    }
}
