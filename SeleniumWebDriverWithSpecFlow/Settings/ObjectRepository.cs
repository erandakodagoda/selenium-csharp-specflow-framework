﻿using OpenQA.Selenium;
using SeleniumWebDriverWithSpecFlow.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumWebDriverWithSpecFlow.Settings
{
    /**
        *  @author : Eranda Kodagoda
        *  @date : September 01, 2020
        *  @version : 1.0
        *  @copyright : © 2020 Eranda Kodagoda
    *   */
    public class ObjectRepository
    {
        public static IConfig Config{get; set;}
        public static IWebDriver Driver{ get; set;}
    }
}
