﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumWebDriverWithSpecFlow.Settings
{

    /**
        *  @author : Eranda Kodagoda
        *  @date : September 01, 2020
        *  @version : 1.0
        *  @copyright : © 2020 Eranda Kodagoda
    *   */
    public class ApplicationConfigKeys
    {
        public const string Browser = "Browser";
        public const string URL = "URL";
        public const string DriverPath = "DriverPath";
    }
}
