﻿using SeleniumWebDriverWithSpecFlow.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumWebDriverWithSpecFlow.ComponentHelper
{
    /**
        *  @author : Eranda Kodagoda
        *  @date : September 01, 2020
        *  @version : 1.0
        *  @copyright : © 2020 Eranda Kodagoda
    *   */
    public class NavigationHelper
    {
        public static void NavigateToURL(string URL)
        {
            ObjectRepository.Driver.Navigate().GoToUrl(URL);
        }
    }
}
