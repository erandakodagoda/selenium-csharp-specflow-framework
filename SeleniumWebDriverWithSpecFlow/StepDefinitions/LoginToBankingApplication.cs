﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using SeleniumWebDriverWithSpecFlow.BaseClasses;
using SeleniumWebDriverWithSpecFlow.ComponentHelper;
using SeleniumWebDriverWithSpecFlow.PageObjects;
using SeleniumWebDriverWithSpecFlow.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace SeleniumWebDriverWithSpecFlow.StepDefinitions
{
    [Binding]
    public sealed class LoginToBankingApplication : BaseClass
    {
        private LoginPageObjects LoginPage = new LoginPageObjects(ObjectRepository.Driver);

        [Given(@"I am in Banking application Login Page")]
        public void GivenIAmInBankingApplicationLoginPage()
        {
            NavigationHelper.NavigateToURL("http://demo.guru99.com/V1/index.php");
        }

        [Given(@"I enter the username (.*)")]
        public void GivenIEnterTheUsername(string user)
        {
            LoginPage.SetUsername(user);
        }

        [Given(@"I enter the password (.*)")]
        public void GivenIEnterThePassword(string pass)
        {
            LoginPage.SetPassword(pass);
        }

        [When(@"I click on the login button")]
        public void WhenIClickOnTheLoginButton()
        {
            LoginPage.ClickLoginBtn();
        }

        [Then(@"I get navigated to Home Page")]
        public void ThenIGetNavigatedToHomePage()
        {
            string menuName = ObjectRepository.Driver.FindElement(By.LinkText("New Customer")).Text;
            Assert.AreEqual(menuName, "New Customer");
        }
    }
}
