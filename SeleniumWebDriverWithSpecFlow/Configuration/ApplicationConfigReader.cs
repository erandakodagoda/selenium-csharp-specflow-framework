﻿using SeleniumWebDriverWithSpecFlow.Interfaces;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using SeleniumWebDriverWithSpecFlow.Settings;

namespace SeleniumWebDriverWithSpecFlow.Configuration
{
    /**
        *  @author : Eranda Kodagoda
        *  @date : September 01, 2020
        *  @version : 1.0
        *  @copyright : © 2020 Eranda Kodagoda
    *   */
    public class ApplicationConfigReader : IConfig
    {
        public BrowserTypes? GetBrowser()
        {
            string browser = ConfigurationManager.AppSettings[ApplicationConfigKeys.Browser]; 
            try
            {
                return (BrowserTypes)Enum.Parse(typeof(BrowserTypes), browser);
            }catch(Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
            
        }

        public string GetURL()
        {
            return ConfigurationManager.AppSettings.Get(ApplicationConfigKeys.URL);
        }
        public string GetDriverPath()
        {
            return ConfigurationManager.AppSettings.Get(ApplicationConfigKeys.DriverPath);
        }

       
    }
}
