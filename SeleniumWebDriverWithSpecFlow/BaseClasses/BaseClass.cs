﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SeleniumWebDriverWithSpecFlow.Configuration;
using SeleniumWebDriverWithSpecFlow.CustomException;
using SeleniumWebDriverWithSpecFlow.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace SeleniumWebDriverWithSpecFlow.BaseClasses
{
    /**
        *  @author : Eranda Kodagoda
        *  @date : September 01, 2020
        *  @version : 1.0
        *  @copyright : © 2020 Eranda Kodagoda
    *   */
    [TestClass]
    [Binding]
    public class BaseClass
    {
        //Setting up Browser Options
        private static FirefoxOptions GetFirefoxOptions()
        {
            FirefoxOptions options = new FirefoxOptions();
            return options;
        }
        private static ChromeOptions GetChromeOptions()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("start-maximized");
            return options;
        }
        private static InternetExplorerOptions GetInternetExplorerOptions()
        {
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            options.EnsureCleanSession = true;
            return options;
        }

        private static IWebDriver GetFirefox()
        {
            ObjectRepository.Config = new ApplicationConfigReader();
            IWebDriver driver = new FirefoxDriver(ObjectRepository.Config.GetDriverPath(), GetFirefoxOptions());
            return driver;
        }
        private static IWebDriver GetChrome()
        {
            ObjectRepository.Config = new ApplicationConfigReader();
            IWebDriver driver = new ChromeDriver(ObjectRepository.Config.GetDriverPath(), GetChromeOptions());
            return driver;
        }
        private static IWebDriver GetIE()
        {
            IWebDriver driver = new InternetExplorerDriver(GetInternetExplorerOptions());
            return driver;
        }

        [BeforeScenario]
        public static void InitWebDriver(TestContext testContext)
        {
            ObjectRepository.Config = new ApplicationConfigReader();
            Console.WriteLine("Driver Name is : {0}",ObjectRepository.Config.GetBrowser());
            switch (ObjectRepository.Config.GetBrowser())
            {
                case BrowserTypes.Firefox:
                    ObjectRepository.Driver = GetFirefox();
                    break;
                case BrowserTypes.Chrome:
                    ObjectRepository.Driver = GetChrome();
                    break;
                case BrowserTypes.InternetExplorer:
                    ObjectRepository.Driver = GetIE();
                    break;
                default:
                    throw new InvalidDriverTypeFoundException("Invalid Driver Name Specified : " + ObjectRepository.Config.GetBrowser().ToString());
            }
        }
        [AfterScenario]
        public static void TearDown()
        {
            if(ObjectRepository.Driver != null)
            {
                ObjectRepository.Driver.Close();
                ObjectRepository.Driver.Quit();
            }
        }

    }
}
