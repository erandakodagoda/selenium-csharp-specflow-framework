﻿using OpenQA.Selenium;
using SeleniumWebDriverWithSpecFlow.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumWebDriverWithSpecFlow.PageObjects
{
    public class LoginPageObjects
    {
        private IWebDriver _driver;
        public LoginPageObjects(IWebDriver driver)
        {
            this._driver = driver;
        }

        #region Elements
        private By usernameTxt = By.Name("uid");
        private By passwordTxt = By.Name("password");
        private By loginBtn = By.Name("btnLogin");
        #endregion

        #region Actions
        public void SetUsername(string username)
        {
            ObjectRepository.Driver.FindElement(usernameTxt).SendKeys(username);

        }
        public void SetPassword(string password)
        {
            ObjectRepository.Driver.FindElement(passwordTxt).SendKeys(password);
        }
        public void ClickLoginBtn()
        {
            ObjectRepository.Driver.FindElement(loginBtn).Click();
        }
        #endregion
    }
}
